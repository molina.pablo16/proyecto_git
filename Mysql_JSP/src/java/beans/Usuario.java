/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;

/**
 *
 * @author admincenec
 */
public class Usuario implements Serializable {

    private String nombre;
    private String contraseña;
    private String email;
    private int nivelAcceso;
    private boolean baneado;

    public Usuario() {
    }

    public Usuario(String nombre, String contraseña, String email, int nivelAcceso, boolean baneado) {
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.email = email;
        this.nivelAcceso = nivelAcceso;
        this.baneado = baneado;
    }

}
