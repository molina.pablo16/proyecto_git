/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PersonalizedTag;

import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author admincenec
 */
public class VerImagen extends SimpleTagSupport{
    
    private StringWriter sw = new StringWriter();
    private String miAttr;
    
    public void setMiAttr(String val){
	this.miAttr= val;
}

    
    @Override
    public void doTag() throws JspException {
        try {
            JspWriter out = getJspContext().getOut();
            getJspBody().invoke(sw);
            out.println("<img class='scaled' src='./media/img/" + sw +"'/> ");
            
            //out.println("<h1>"+sw+"<h1>");
            
            if(miAttr!=null){out.println(miAttr);}


        } catch (IOException ex) {
            Logger.getLogger(HolaMundo.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        
        
        
    }}
