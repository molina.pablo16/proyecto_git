/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PersonalizedTag;

import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author admincenec
 */
public class HolaMundo extends SimpleTagSupport {
    
    private StringWriter sw = new StringWriter();

    
    @Override
    public void doTag() throws JspException {
        try {
            JspWriter out = getJspContext().getOut();
            out.println("<img src='https://media.giphy.com/media/bn0zlGb4LOyo8/giphy.gif'/> ");
            getJspBody().invoke(sw);
            out.println("<h1>"+sw+"<h1>");
            
          


        } catch (IOException ex) {
            Logger.getLogger(HolaMundo.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        
        
        
    }
}
