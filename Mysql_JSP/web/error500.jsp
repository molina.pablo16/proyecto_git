<%-- 
    Document   : error404.jsp
    Created on : 13-oct-2018, 21:32:10
    Author     : PMM
--%>

<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.StringWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--<%@ page isErrorPage="true" %> --%>



<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>500 INTERNAL SERVER ERROR</title>


    <link type="text/css" rel="stylesheet" href="./styles/style.css" />



</head>

<body class="error500">
    
    <div class="error500">

        <h1>INTERNAL SERVER ERROR</h1>
        <img src="./media/img/500cage.jpg" alt="500cage" title="500cage"/>
        <p>The server encountered an internal error or missconfiguration and was unable to complete your request. <br>
            Please contact the server administrator, webmaster@example.com and inform then of the time the error occurred, and anything you might hae done that may have the error. <br>
            More information about this error may be available in the server error log or below.
        </p>
    </div>

    Message:<%=exception.getMessage()%>
    StackTrace:
    <%
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        out.println(stringWriter);
        printWriter.close();
        stringWriter.close();
    %>

    

</body>
