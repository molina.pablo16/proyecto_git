<%-- 
    Document   : error404.jsp
    Created on : 13-oct-2018, 21:32:10
    Author     : PMM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>404 ERROR</title>


	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="./styles/style.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body class="error404">

    <div class="error404">
				<h1>ERROR 404</h1>
			
			<h2>Oops! This <b>C</b>age Could Not Be Found</h2>
			<p>Sorry but the <b>C</b>age you are looking for does not exist, have been removed, name changed or is temporarily unavailable</p>
		</div>

</body>
